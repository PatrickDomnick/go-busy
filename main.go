package main

import (
	"gitlab.com/PatrickDomnick/go-busy/cmd"
)

var version = "latest"

func main() {
	cmd.Execute(version)
}
