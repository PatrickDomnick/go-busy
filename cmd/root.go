package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/PatrickDomnick/go-busy/pkg/logger"
)

var (
	logs      logger.Logger
	debugMode bool
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "go-busy",
	Short: "A little keyboard input simulator to do various tasks",
	Long:  `Simulate Keypress Events to fool a system, appear more busy then you are or simply keep that teams bubble from turning yellow.`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		logs = logger.Create(debugMode)
		if !debugMode {
			os.Stderr, _ = os.Open(os.DevNull)
		}
		initializeConfig(cmd)
	},
}

func initializeConfig(cmd *cobra.Command) {
	v := viper.New()

	// Set as many paths as you like where viper should look for the
	// config file. We are only looking in the current working directory.
	v.AddConfigPath(".")

	// Attempt to read the config file, gracefully ignoring errors
	// caused by a config file not being found. Return an error
	// if we cannot parse the config file.
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			logs.Fatal(err)
		}
	}

	// When we bind flags to environment variables expect that the
	// environment variables are prefixed. This helps avoid conflicts.
	v.SetEnvPrefix("GO_BUSY")

	// Bind to environment variables
	// Works great for simple config names, but needs help for names
	// with dashes which we fix in the bindFlags function
	v.AutomaticEnv()

	// Bind the current command's flags to viper
	bindFlags(cmd, v)
}

// Bind each cobra flag to its associated viper configuration (config file and environment variable)
func bindFlags(cmd *cobra.Command, v *viper.Viper) {
	cmd.Flags().VisitAll(func(f *pflag.Flag) {
		// Environment variables can't have dashes in them, so bind them to their equivalent
		if strings.Contains(f.Name, "-") {
			envVarSuffix := strings.ToUpper(strings.ReplaceAll(f.Name, "-", "_"))
			v.BindEnv(f.Name, fmt.Sprintf("%s_%s", "GO_BUSY", envVarSuffix))
		}

		// Apply the viper config value to the flag when the flag is not set and viper has a value
		if !f.Changed && v.IsSet(f.Name) {
			val := v.Get(f.Name)
			cmd.Flags().Set(f.Name, fmt.Sprintf("%s", val))
		}
	})
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(mainVersion string) {
	version = mainVersion

	// Overwrite default CMD
	cmd, _, err := rootCmd.Find(os.Args[1:])
	// default cmd if no cmd is given
	if err == nil && cmd.Use == rootCmd.Use && cmd.Flags().Parse(os.Args[1:]) != pflag.ErrHelp {
		args := append([]string{keyboardCmd.Use, pressKeyboardCmd.Use}, os.Args[1:]...)
		rootCmd.SetArgs(args)
	}

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	// Debugging
	rootCmd.PersistentFlags().BoolVar(&debugMode, "debug", false, "Turn on debug logging.")
}
