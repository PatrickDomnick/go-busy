package cmd

import (
	"github.com/spf13/cobra"
)

var keys []int

var pressKeyboardCmd = &cobra.Command{
	Use:     "press",
	Short:   "Execute Keyboard Press Events",
	Long:    "Execute predefined Keyboard Events based on the configuration.",
	Example: "go-busy keyboard press",
	Run: func(cmd *cobra.Command, args []string) {
		kb.Execute(keys)
	},
}

// https://pkg.go.dev/github.com/micmonay/keybd_event@v1.1.1#pkg-constants
func init() {
	keyboardCmd.PersistentFlags().IntSliceVarP(&keys, "keys", "k", []int{57}, "Which Key to Press")
	keyboardCmd.AddCommand(pressKeyboardCmd)
}
