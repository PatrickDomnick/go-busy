package cmd

import (
	"github.com/spf13/cobra"
)

var (
	version    = "latest"
	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Version will output the current build version",
		Long:  ``,
		Run: func(_ *cobra.Command, _ []string) {
			logs.Print(version)
		},
	}
)

func init() {
	rootCmd.AddCommand(versionCmd)
}
