package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

// generateDocsCmd represents the generateDocs command
var generateDocsCmd = &cobra.Command{
	Use:   "generateDocs",
	Short: "A brief description of your command",
	Run: func(cmd *cobra.Command, args []string) {
		err := doc.GenMarkdownTree(rootCmd, "./docs")
		if err != nil {
			logs.Fatal(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(generateDocsCmd)
}
