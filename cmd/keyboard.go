package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/PatrickDomnick/go-busy/pkg/keyboard"
)

// Global Configuration Variables
var (
	kb           *keyboard.Keyboard
	delay        int
	pressedDelay int
	nextDelay    int
	loop         bool
)

var keyboardCmd = &cobra.Command{
	Use:   "keyboard",
	Short: "Initialize Keyboard Settings",
	Long:  "This will initialize the keyboard and set global settings",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		logs.Debug("Initializing the Keyboard Settings...")
		kb = keyboard.Create(delay, pressedDelay, nextDelay, loop)
	},
}

func init() {
	keyboardCmd.PersistentFlags().IntVarP(&delay, "delay", "d", 300, "Seconds to wait before executing keypress events")
	keyboardCmd.PersistentFlags().IntVarP(&pressedDelay, "pressedDelay", "p", 100, "Milliseconds to wait before releasing a keypress events")
	keyboardCmd.PersistentFlags().IntVarP(&nextDelay, "nextDelay", "n", 100, "Milliseconds to wait before pressing the next key")
	keyboardCmd.PersistentFlags().BoolVarP(&loop, "loop", "l", true, "Loop the event when finished")
	// Add Command to Structure
	rootCmd.AddCommand(keyboardCmd)
}
