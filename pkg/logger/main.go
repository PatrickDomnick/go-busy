package logger

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/logrusorgru/aurora"
)

// Logger The logger
type Logger struct {
	debug bool
}

// Create Create the logger
func Create(debug bool) Logger {
	return Logger{debug}
}

// Fatal Log fatal error (exit the program) if error not nil
func (l Logger) Fatal(err error) {
	log.Fatal(aurora.Red(err))
}

// Success Log success message
func (l Logger) Success(s string) {
	log.Println(aurora.Green(s))
}

// Debug Write debug messages
func (l Logger) Debug(s string) {
	if l.debug {
		log.Println(s)
	}
}

// Print Write something...
func (l Logger) Print(s string) {
	log.Println(s)
}

// PrintAsJSON Try to convert data into json and print it
func (l Logger) PrintAsJSON(i interface{}) {
	jsonData, err := json.MarshalIndent(i, "", "  ")
	if err != nil {
		l.Fatal(err)
	}
	fmt.Println(string(jsonData))
}
