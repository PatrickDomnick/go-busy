package keyboard

import (
	"time"

	"github.com/micmonay/keybd_event"
)

// Logger The logger
type Keyboard struct {
	kb           keybd_event.KeyBonding
	delay        int
	pressedDelay int
	nextDelay    int
	loop         bool
}

// Create Create the logger
func Create(delay int, pressedDelay int, nextDelay int, loop bool) *Keyboard {
	kb, err := keybd_event.NewKeyBonding()
	if err != nil {
		panic(err)
	}

	return &Keyboard{
		kb:           kb,
		delay:        delay,
		pressedDelay: pressedDelay,
		nextDelay:    nextDelay,
		loop:         loop,
	}
}

func (k *Keyboard) Execute(keys []int) {
	iterator := 0
	if k.loop {
		iterator = 2 // Will run forever
	}
	for iterator != 1 {
		time.Sleep(time.Duration(k.delay) * time.Second)
		for _, key := range keys {
			k.Press(key)
		}
		iterator++
	}
}

func (k *Keyboard) Press(key int) {
	// Select keys to be pressed
	k.kb.SetKeys(key)

	// Press and Release
	k.kb.Press()
	time.Sleep(time.Duration(k.pressedDelay) * time.Millisecond)
	k.kb.Release()

	// Wait for the next input event
	time.Sleep(time.Duration(k.nextDelay) * time.Millisecond)
}
