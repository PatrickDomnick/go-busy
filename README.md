# Go Busy

> Simulate Keypress Events to fool a system, appear more busy then you are or simply keep that teams bubble from turning yellow.

## TLDR

Downloading and executing this without any modifications will simply hit the space bar every 5 Minutes until you kill the program.

## Advanced Usage

Check out the [Documentation](/docs/go-busy.md).

## Authors and acknowledgment

Thanks to:

- [Michaël Monayron - Keypress Library](https://github.com/micmonay)
- [Steve Francia - Cobra](https://spf13.com/)

## License

MIT
